import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { resolve } from 'path'

const rootDir = resolve(__dirname)
const srcDir = resolve(rootDir, 'src')
const outDir = resolve(rootDir, 'dist')

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@shared': resolve(srcDir, 'shared')
    }
  },
  plugins: [react()],
  build: {
    outDir,
    rollupOptions: {
      input: {
        content: resolve(srcDir, 'pages', 'content', 'index.tsx'),
        background: resolve(srcDir, 'pages', 'background', 'index.ts'),
      },
      output: {
        entryFileNames: 'src/pages/[name]/index.js',
        chunkFileNames: 'assets/js/[name].js',
      }
    }
  }
})